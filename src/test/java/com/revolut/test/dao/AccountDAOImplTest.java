package com.revolut.test.dao;

import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.revolut.test.api.dao.AccountDAO;
import com.revolut.test.db.PersistInitialiser;
import com.revolut.test.exception.ExternalException;
import com.revolut.test.model.AccountBO;
import com.revolut.test.model.AccountStatus;
import java.math.BigDecimal;
import java.util.Properties;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class AccountDAOImplTest {

    private static final Long ACCOUNT_ID = 10000l;

    @Inject
    private AccountDAO dao;

    @Before
    public void setup() {
        Injector injector = Guice.createInjector(new Module() {
            @Override
            public void configure(Binder binder) {
                binder.install(jpaModule());
                binder.bind(AccountDAO.class).to(AccountDAOImpl.class);
            }
        });
        injector.getInstance(PersistInitialiser.class);
        injector.injectMembers(this);
    }

    private Module jpaModule() {
        final Properties properties = new Properties();
        properties.put("javax.persistence.jdbc.driver", "org.h2.Driver");
        properties.put("javax.persistence.jdbc.url", "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1");
        properties.put("javax.persistence.jdbc.user", "sa");
        properties.put("javax.persistence.jdbc.password", "sa");
        final JpaPersistModule jpaModule = new JpaPersistModule("UnitTest");
        jpaModule.properties(properties);
        return jpaModule;
    }

    @Test
    public void testShouldBeAbleToCreateAnAccount() throws ExternalException {
        Assert.assertNotNull(dao.create(getAccountBO()));
    }

    private AccountBO getAccountBO() {
        return AccountBO.builder().id(ACCOUNT_ID).firstName("Pankajpreet").lastName("Singh").email(
            "pankajpreet86@gmail.com").mobileNumber("88888888888").balance(
            BigDecimal.TEN).status(AccountStatus.INACTIVE).build();
    }

    @Test
    public void testShouldBeAbleToFindAnAccountById() throws ExternalException {
        AccountBO accountBO = getAccountBO();
        dao.create(accountBO);
        AccountBO accountBODB = dao.findById(accountBO.getId());
        Assert.assertEquals(accountBO, accountBODB);
    }

    @Test
    public void testShouldBeAbleToDoDebitOperation() throws ExternalException {
        AccountBO accountBO = getAccountBO();
        dao.create(accountBO);
        AccountBO accountBODB = dao.findById(accountBO.getId());
        dao.debit(accountBODB.getId(), BigDecimal.valueOf(2.5));
        AccountBO updatedAccountBODB = dao.findById(accountBO.getId());
        Assert.assertEquals(BigDecimal.valueOf(7.5), updatedAccountBODB.getBalance());
    }

    @Test(expected = ExternalException.class)
    public void testShouldThrowExceptionIfBalanceThresholdReached() throws ExternalException {
        AccountBO accountBO = getAccountBO();
        dao.create(accountBO);
        try {
            dao.debit(accountBO.getId(), BigDecimal.valueOf(15));
        } catch (ExternalException e) {
            Assert.assertEquals("Transfer Failed: Insufficient funds", e.getMessage());
            throw e;
        }
    }

    @Test
    public void testShouldNotUpdateBalanceWhenMinBalanceThresholdReached() throws ExternalException {
        AccountBO accountBO = getAccountBO();
        dao.create(accountBO);
        AccountBO accountBODB = dao.findById(accountBO.getId());
        try {
            dao.debit(accountBO.getId(), BigDecimal.valueOf(15));
        } catch (ExternalException e) {
            //No op
        }
        AccountBO accountBODBUpdated = dao.findById(accountBO.getId());
        Assert.assertEquals(accountBODB.getBalance(), accountBODBUpdated.getBalance());
    }

    @Test
    public void testShouldBeAbleToDoCreditOperation() throws ExternalException {
        AccountBO accountBO = getAccountBO();
        dao.create(accountBO);
        AccountBO accountBODB = dao.findById(accountBO.getId());
        dao.credit(accountBODB.getId(), BigDecimal.valueOf(2.5));
        AccountBO updatedAccountBODB = dao.findById(accountBO.getId());
        Assert.assertEquals(BigDecimal.valueOf(12.5), updatedAccountBODB.getBalance());
    }

}
