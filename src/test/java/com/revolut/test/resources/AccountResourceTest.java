package com.revolut.test.resources;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.revolut.test.api.AccountManager;
import com.revolut.test.exception.ExternalException;
import com.revolut.test.model.AccountBO;
import com.revolut.test.model.AccountStatus;
import com.revolut.test.model.TransferBO;
import com.revolut.test.resources.input.AccountRO;
import com.revolut.test.resources.input.TransferRO;
import com.revolut.test.resources.output.AccountResponse;
import com.revolut.test.resources.output.Errors;
import com.revolut.test.resources.output.TransferResponse;
import java.math.BigDecimal;
import javax.ws.rs.core.Response;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class AccountResourceTest {

    private static final Long FROM_ACCOUNT = 1000l;
    private static final Long TO_ACCOUNT = 10001l;
    //
    @Inject
    private AccountManager accountManagerMock;

    private AccountResource accountResource;

    @Before
    public void setup() {
        Injector injector = Guice.createInjector(new Module() {
            @Override
            public void configure(Binder binder) {
                binder.bind(AccountManager.class).toInstance(mock(AccountManager.class));
            }
        });
        injector.injectMembers(this);
        accountResource = injector.getInstance(AccountResource.class);
    }

    @Test
    public void testCreateAccount() throws ExternalException {
        Mockito.when(accountManagerMock.createAccount(any(AccountBO.class))).thenReturn(getAccountBO());
        Response response = accountResource.createAccount(getAccountRO());
        Assert.assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
        AccountResponse responseEntity = (AccountResponse) response.getEntity();
        Assert.assertEquals("Pankajpreet", responseEntity.getFirstName());
        Assert.assertEquals("Singh", responseEntity.getLastName());
        Assert.assertEquals("abs@anc.com", responseEntity.getEmail());
        Assert.assertEquals("+447405277722", responseEntity.getMobileNumber());
        Assert.assertEquals(1l, responseEntity.getId().longValue());
    }

    @Test
    public void testCreateAccountWithFirstNameNull() throws ExternalException {
        AccountRO accountRO = getAccountRO();
        accountRO.setFirstName(null);
        Response response = accountResource.createAccount(accountRO);
        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void testCreateAccountWithFirstNameEmptyString() throws ExternalException {
        AccountRO accountRO = getAccountRO();
        accountRO.setFirstName("");
        Response response = accountResource.createAccount(accountRO);
        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void testCreateAccountWithLastNameNull() throws ExternalException {
        AccountRO accountRO = getAccountRO();
        accountRO.setLastName(null);
        Response response = accountResource.createAccount(accountRO);
        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void testCreateAccountWithLastNameEmptyString() throws ExternalException {
        AccountRO accountRO = getAccountRO();
        accountRO.setLastName("");
        Response response = accountResource.createAccount(accountRO);
        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void testCreateAccountWithEmailAsInvalidValue() throws ExternalException {
        AccountRO accountRO = getAccountRO();
        accountRO.setEmail("aaa");
        Response response = accountResource.createAccount(accountRO);
        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void testCreateAccountWithMandatoryParams() throws ExternalException {
        Mockito.when(accountManagerMock.createAccount(any(AccountBO.class))).thenReturn(getAccountBO());
        AccountRO accountRO = getAccountRO();
        accountRO.setEmail(null);
        accountRO.setMobileNumber(null);
        Response response = accountResource.createAccount(accountRO);
        Assert.assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
    }

    @Test
    public void testCreateAccountFailed() throws ExternalException {
        Mockito.when(accountManagerMock.createAccount(any(AccountBO.class))).thenThrow(
            new ExternalException("Test error"));
        Response response = accountResource.createAccount(getAccountRO());
        Assert.assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
        Errors responseEntity = (Errors) response.getEntity();
        Assert.assertEquals(1, responseEntity.getData().size());
        Assert.assertEquals(500, responseEntity.getData().get(0).getErrorCode());
        Assert.assertEquals("Test error", responseEntity.getData().get(0).getErrorMsg());
    }

    @Test
    public void testGetAccountWhenAccountDoesNotExist() throws ExternalException {
        Mockito.when(accountManagerMock.getAccoundById(any(Long.class))).thenReturn(null);
        Response response = accountResource.getAccount(1l);
        Assert.assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
    }

    @Test
    public void testGetAccountWithValidAccountId() throws ExternalException {
        AccountBO accountBO = getAccountBO();
        Mockito.when(accountManagerMock.getAccoundById(any(Long.class))).thenReturn(accountBO);
        Response response = accountResource.getAccount(1l);
        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        AccountResponse responseEntity = (AccountResponse) response.getEntity();
        Assert.assertEquals(accountBO.getFirstName(), responseEntity.getFirstName());
        Assert.assertEquals(accountBO.getLastName(), responseEntity.getLastName());
        Assert.assertEquals(accountBO.getEmail(), responseEntity.getEmail());
        Assert.assertEquals(accountBO.getMobileNumber(), responseEntity.getMobileNumber());
        Assert.assertEquals(accountBO.getId(), responseEntity.getId());
    }

    @Test
    public void testErroResponseWhenGetAccountFailed() throws ExternalException {
        Mockito.when(accountManagerMock.getAccoundById(any(Long.class))).thenThrow(new ExternalException("Test error"));
        Response response = accountResource.getAccount(1l);
        Assert.assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
        Errors responseEntity = (Errors) response.getEntity();
        Assert.assertEquals(1, responseEntity.getData().size());
        Assert.assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(),
            responseEntity.getData().get(0).getErrorCode());
        Assert.assertEquals("Test error", responseEntity.getData().get(0).getErrorMsg());
    }

    @Test
    public void testAccountTransferSuccess() throws ExternalException {
        Mockito.when(accountManagerMock.getAccoundById(any())).thenReturn(getAccountBO());
        TransferBO transferBO = getTransferBO();
        when(accountManagerMock.transfer(any(TransferBO.class))).thenReturn(transferBO);
        Response response = accountResource.transfer(getTransferRO());
        Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
        TransferResponse entity = (TransferResponse) response.getEntity();
        Assert.assertEquals(transferBO.getTransactionId(), entity.getTransactionId());
        Assert.assertEquals(transferBO.getAvailableBalance(), entity.getAvailableBalance());
        Assert.assertEquals(transferBO.getAmount(), entity.getAmmount());
        Assert.assertEquals(transferBO.getFromAccount(), entity.getFromAccount());
        Assert.assertEquals(transferBO.getToAccount(), entity.getToAccount());
        Assert.assertEquals(transferBO.getMsg(), entity.getMsg());
    }
    @Test
    public void testAccountTransferReturnProperErrorWhenEcxeptionIsThrown() throws ExternalException {
        Mockito.when(accountManagerMock.getAccoundById(any())).thenReturn(getAccountBO());
        TransferBO transferBO = getTransferBO();
        when(accountManagerMock.transfer(any(TransferBO.class))).thenThrow(new ExternalException("Test Error"));
        Response response = accountResource.transfer(getTransferRO());
        Assert.assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
        Errors responseEntity = (Errors) response.getEntity();
        Assert.assertEquals(1, responseEntity.getData().size());
        Assert.assertEquals(500, responseEntity.getData().get(0).getErrorCode());
        Assert.assertEquals("Test Error", responseEntity.getData().get(0).getErrorMsg());
    }

    @Test
    public void testAccountTransferWithFromAccountNull() throws ExternalException {
        TransferRO transferRO = getTransferRO();
        transferRO.setFromAccount(null);
        Response response = accountResource.transfer(transferRO);
        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void testAccountTransferWithToAccountNull() throws ExternalException {
        TransferRO transferRO = getTransferRO();
        transferRO.setToAccount(null);
        Response response = accountResource.transfer(transferRO);
        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void testAccountTransferWithAmountNull() throws ExternalException {
        TransferRO transferRO = getTransferRO();
        transferRO.setAmount(null);
        Response response = accountResource.transfer(transferRO);
        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void testAccountTransferWithMsgNull() throws ExternalException {
        TransferRO transferRO = getTransferRO();
        transferRO.setMsg(null);
        Response response = accountResource.transfer(transferRO);
        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    @Test
    public void testAccountTransferWithMsgAsEmpty() throws ExternalException {
        TransferRO transferRO = getTransferRO();
        transferRO.setMsg("");
        Response response = accountResource.transfer(transferRO);
        Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
    }

    private TransferBO getTransferBO() {
        return TransferBO.builder().transactionId(10000l).msg("Test").toAccount(FROM_ACCOUNT).toAccount(TO_ACCOUNT)
            .availableBalance(BigDecimal.TEN).amount(BigDecimal.TEN).build();
    }

    private TransferRO getTransferRO() {
        TransferRO transferRO = new TransferRO();
        transferRO.setAmount(BigDecimal.TEN);
        transferRO.setToAccount(TO_ACCOUNT);
        transferRO.setFromAccount(FROM_ACCOUNT);
        transferRO.setMsg("Test");
        return transferRO;
    }

    private AccountRO getAccountRO() {
        AccountRO accountRO = new AccountRO();
        accountRO.setFirstName("Pankajpreet");
        accountRO.setLastName("Singh");
        accountRO.setEmail("abs@anc.com");
        accountRO.setMobileNumber("+447405277722");
        return accountRO;
    }

    private AccountBO getAccountBO() {
        return AccountBO.builder().id(1l).firstName("Pankajpreet").lastName("Singh").email("abs@anc.com").mobileNumber(
            "+447405277722").status(AccountStatus.ACTIVE).build();
    }

}
