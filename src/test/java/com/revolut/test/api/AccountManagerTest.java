package com.revolut.test.api;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import com.google.inject.Binder;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.revolut.test.api.dao.AccountDAO;
import com.revolut.test.core.account.AccountManagerImpl;
import com.revolut.test.exception.ExternalException;
import com.revolut.test.model.AccountBO;
import com.revolut.test.model.AccountStatus;
import com.revolut.test.model.TransferBO;
import java.math.BigDecimal;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

public class AccountManagerTest {

    private static final Long FROM_ACCOUNT = 1000l;
    private static final Long TO_ACCOUNT = 10001l;
    //
    @Inject
    private AccountManager accountManager;
    @Inject
    private AccountDAO accountDAOMock;

    @Before
    public void setup() {
        Injector injector = Guice.createInjector(new Module() {
            @Override
            public void configure(Binder binder) {
                binder.bind(AccountManager.class).to(AccountManagerImpl.class);
                binder.bind(AccountDAO.class).toInstance(mock(AccountDAO.class));
            }
        });
        injector.injectMembers(this);
    }

    @Test
    public void testAccountCreation() throws ExternalException {
        when(accountDAOMock.create(any())).thenReturn(getAccountBO());
        accountManager.createAccount(getAccountBO());
        ArgumentCaptor<AccountBO> accountBOArgumentCaptor = ArgumentCaptor.forClass(AccountBO.class);
        verify(accountDAOMock, times(1)).create(accountBOArgumentCaptor.capture());
        AccountBO value = accountBOArgumentCaptor.getValue();
        Assert.assertNotNull(value.getId());
        Assert.assertNotNull(value.getBalance());
        Assert.assertNotNull(value.getStatus());
        Assert.assertEquals(AccountStatus.ACTIVE, value.getStatus());
    }

    @Test(expected = ExternalException.class)
    public void testAccountCreationDAOException() throws ExternalException {
        when(accountDAOMock.create(any())).thenThrow(new ExternalException("Test Error"));
        accountManager.createAccount(getAccountBO());
    }

    @Test(expected = ExternalException.class)
    public void testGetAccountDAOException() throws ExternalException {
        when(accountDAOMock.findById(any())).thenThrow(new ExternalException("Test Error"));
        accountManager.getAccoundById(1l);
    }

    @Test
    public void testGetAccount() throws ExternalException {
        when(accountDAOMock.findById(any())).thenReturn(getAccountBO());
        AccountBO accoundById = accountManager.getAccoundById(1l);
        Assert.assertNotNull(accoundById);
        Assert.assertEquals(getAccountBO(), accoundById);
    }

    @Test(expected = ExternalException.class)
    public void testTransferOperationWhenDebitOperationThrowsException() throws ExternalException {
        when(accountDAOMock.debit(any(), any())).thenThrow(new ExternalException("Test Error"));
        accountManager.transfer(getTransferBO());
    }

    @Test(expected = ExternalException.class)
    public void testTransferOperationWhenCreditOperationThrowsException() throws ExternalException {
        when(accountDAOMock.credit(any(), any())).thenThrow(new ExternalException("Test Error"));
        accountManager.transfer(getTransferBO());
    }

    @Test
    public void testSuccessfulTransferOperation() throws ExternalException {
        when(accountDAOMock.debit(any(), any())).thenReturn(getAccountBO());
        when(accountDAOMock.credit(any(), any())).thenReturn(getAccountBO());
        TransferBO transferBO = accountManager.transfer(getTransferBO());
        verify(accountDAOMock, times(1)).credit(any(), any());
        verify(accountDAOMock, times(1)).debit(any(), any());
        Assert.assertNotNull(transferBO);
        Assert.assertNotNull(transferBO.getTransactionId());
        Assert.assertNotNull(transferBO.getAvailableBalance());
    }

    private AccountBO getAccountBO() {
        return AccountBO.builder().firstName("Pankajpreet").lastName("Singh").email("abs@anc.com").mobileNumber(
            "+447405277722").balance(BigDecimal.TEN).status(AccountStatus.ACTIVE).build();
    }

    private TransferBO getTransferBO() {
        return TransferBO.builder().msg("Test").toAccount(FROM_ACCOUNT).toAccount(TO_ACCOUNT)
            .amount(BigDecimal.TEN).build();
    }
}
