package com.revolut.test.core.util;

import java.util.concurrent.atomic.AtomicLong;

public class IdGenerator {

    private static final IdGenerator INSTANCE = new IdGenerator();

    private AtomicLong accountIdCounter = new AtomicLong(0);
    private AtomicLong transactionIdCounter = new AtomicLong(50000);


    private IdGenerator() {
        //No op
    }

    public static IdGenerator getInstance() {
        return INSTANCE;
    }

    public Long getAccountId() {
        return accountIdCounter.incrementAndGet();
    }

    public Long getTransactionId() {
        return transactionIdCounter.incrementAndGet();
    }

}
