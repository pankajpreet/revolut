package com.revolut.test.core.account;

import com.revolut.test.api.AccountManager;
import com.revolut.test.api.dao.AccountDAO;
import com.revolut.test.core.util.IdGenerator;
import com.revolut.test.exception.ExternalException;
import com.revolut.test.model.AccountBO;
import com.revolut.test.model.AccountStatus;
import com.revolut.test.model.TransferBO;
import java.math.BigDecimal;
import javax.inject.Inject;
import javax.transaction.Transactional;

public class AccountManagerImpl implements AccountManager {

    @Inject
    private AccountDAO accountDAO;

    @Override
    @Transactional
    public AccountBO createAccount(AccountBO accountBO) throws ExternalException {
        // ID generation will be moved to dbase sequence
        accountBO.setId(IdGenerator.getInstance().getAccountId());
        //There are no main accounts, for now adding default balance for all accounts
        accountBO.setBalance(BigDecimal.TEN);
        //By Default account should be INACTIVE and then another REST request to make the account ACTIVE
        // These are done to keep it simple.
        accountBO.setStatus(AccountStatus.ACTIVE);
        return accountDAO.create(accountBO);
    }

    @Override
    @Transactional
    public AccountBO getAccoundById(Long accountNumber) throws ExternalException {
        return accountDAO.findById(accountNumber);
    }

    @Override
    @Transactional
    public TransferBO transfer(TransferBO transferBO) throws ExternalException {
        // Here not checking null considering all validation are already done at REST layer like,
        // account exist
        // account in valid state.
        AccountBO accountBO = accountDAO.debit(transferBO.getFromAccount(), transferBO.getAmount());
        accountDAO.credit(transferBO.getToAccount(), transferBO.getAmount());
        transferBO.setTransactionId(IdGenerator.getInstance().getTransactionId());
        transferBO.setAvailableBalance(accountBO.getBalance());
        return transferBO;
    }
}
