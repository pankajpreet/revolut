package com.revolut.test.model;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TransferBO {
    private Long fromAccount;
    private Long toAccount;
    private BigDecimal amount;
    private String msg;
    private Long transactionId;
    private BigDecimal availableBalance;
}
