package com.revolut.test.model;

public enum AccountStatus {
    INACTIVE,
    ACTIVE,
    SUSPENDED,
    CLOSED
}
