package com.revolut.test.model;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class AccountBO {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String mobileNumber;
    private BigDecimal balance;
    private AccountStatus status;
}
