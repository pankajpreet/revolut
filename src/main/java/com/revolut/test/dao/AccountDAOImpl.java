package com.revolut.test.dao;

import com.google.inject.Inject;
import com.revolut.test.api.dao.AccountDAO;
import com.revolut.test.dao.entity.AccountDO;
import com.revolut.test.dao.entity.mapper.AccountBODOMapper;
import com.revolut.test.exception.ExternalException;
import com.revolut.test.model.AccountBO;
import java.math.BigDecimal;
import javax.persistence.EntityManager;

public class AccountDAOImpl implements AccountDAO {

    private static final int ACCOUNT_MIN_BALANCE_THRESHOLD = 0;
    @Inject
    private EntityManager entityManager;

    @Override
    public AccountBO create(AccountBO accountBO) throws ExternalException {
        AccountDO accountDO = AccountBODOMapper.map(accountBO);
        entityManager.persist(accountDO);
        return accountBO;
    }

    @Override
    public AccountBO findById(Long id) throws ExternalException {
        AccountDO accountDO = getAccountDO(id);
        if (accountDO == null) {
            return null;
        }
        return AccountBODOMapper.mapReverse(accountDO);
    }

    private AccountDO getAccountDO(Long id) {
        AccountDO accountDO = entityManager.find(AccountDO.class, id);
        if (null == accountDO) {
            return null;
        }
        return accountDO;
    }

    @Override
    public AccountBO debit(Long accountId, BigDecimal ammount) throws ExternalException {
        AccountDO accountDO = getAccountDO(accountId);
        BigDecimal updatedBalance = accountDO.getBalance().subtract(ammount);
        if (updatedBalance.compareTo(BigDecimal.ZERO) < ACCOUNT_MIN_BALANCE_THRESHOLD) {
            throw new ExternalException("Transfer Failed: Insufficient funds");
        }
        accountDO.setBalance(updatedBalance);
        AccountDO accountDODB = entityManager.merge(accountDO);
        return AccountBODOMapper.mapReverse(accountDODB);
    }

    @Override
    public AccountBO credit(Long accountId, BigDecimal ammount) throws ExternalException {
        AccountDO accountDO = getAccountDO(accountId);
        BigDecimal updatedBalance = accountDO.getBalance().add(ammount);
        accountDO.setBalance(updatedBalance);
        AccountDO accountDODB = entityManager.merge(accountDO);
        return AccountBODOMapper.mapReverse(accountDODB);
    }


}
