package com.revolut.test.dao.entity.mapper;

import com.revolut.test.dao.entity.AccountDO;
import com.revolut.test.model.AccountBO;
import com.revolut.test.model.AccountStatus;

public class AccountBODOMapper {
    public static AccountDO map(AccountBO accountBO) {
        return AccountDO.build(accountBO);
    }

    public static AccountBO mapReverse(AccountDO accountDO) {
        return AccountBO.builder().id(accountDO.getId()).firstName(accountDO.getFirstName())
            .lastName(accountDO.getLastName()).email(accountDO.getEmail()).mobileNumber(accountDO.getMobileNumber())
            .balance(accountDO.getBalance()).status(AccountStatus.valueOf(accountDO.getStatus())).build();
    }

}
