package com.revolut.test.dao.entity;

import com.revolut.test.model.AccountBO;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;


@Table(name = "account")
@Entity
public class AccountDO implements Serializable {
    @Id
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String mobileNumber;
    private BigDecimal balance;
    private String status;

    public static AccountDO build(AccountBO accountBO) {
        AccountDO accountDO = new AccountDO();
        accountDO.setId(accountBO.getId());
        accountDO.setBalance(accountBO.getBalance());
        accountDO.setEmail(accountBO.getEmail());
        accountDO.setMobileNumber(accountBO.getMobileNumber());
        accountDO.setFirstName(accountBO.getFirstName());
        accountDO.setLastName(accountBO.getLastName());
        accountDO.setStatus(accountBO.getStatus().name());
        return accountDO;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getStatus() {
        return status;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
