package com.revolut.test.api.dao;

import com.revolut.test.exception.ExternalException;
import com.revolut.test.model.AccountBO;
import java.math.BigDecimal;

public interface AccountDAO {
    public AccountBO create(AccountBO accountBO) throws ExternalException;

    AccountBO findById(Long id) throws ExternalException;

    AccountBO debit(Long accountId, BigDecimal ammount) throws ExternalException;

    AccountBO credit(Long accountId, BigDecimal ammount) throws ExternalException;
}
