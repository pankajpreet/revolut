package com.revolut.test.api;

import com.revolut.test.exception.ExternalException;
import com.revolut.test.model.AccountBO;
import com.revolut.test.model.TransferBO;

public interface AccountManager {

    AccountBO createAccount(AccountBO accountBO) throws ExternalException;

    AccountBO getAccoundById(Long accountNumber) throws ExternalException;

    TransferBO transfer(TransferBO map) throws ExternalException;
}
