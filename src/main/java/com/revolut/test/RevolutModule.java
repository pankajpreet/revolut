package com.revolut.test;

import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.revolut.test.api.AccountManager;
import com.revolut.test.api.dao.AccountDAO;
import com.revolut.test.core.account.AccountManagerImpl;
import com.revolut.test.dao.AccountDAOImpl;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.setup.Environment;
import java.util.Properties;

public class RevolutModule extends AbstractModule {

    private final RevolutConfiguration configuration;
    private final Environment environment;

    public RevolutModule(final RevolutConfiguration configuration, final Environment environment) {
        this.configuration = configuration;
        this.environment = environment;
    }

    @Override
    protected void configure() {
        bind(AccountManager.class).to(AccountManagerImpl.class);
        bind(AccountDAO.class).to(AccountDAOImpl.class);

        install(jpaModule(this.configuration.getDataSourceFactory()));
    }

    private Module jpaModule(DataSourceFactory dataSourceFactory) {
        final Properties properties = new Properties();
        properties.put("javax.persistence.jdbc.driver", dataSourceFactory.getDriverClass());
        properties.put("javax.persistence.jdbc.url", dataSourceFactory.getUrl());
        properties.put("javax.persistence.jdbc.user", dataSourceFactory.getUser());
        properties.put("javax.persistence.jdbc.password", dataSourceFactory.getPassword());

        final JpaPersistModule jpaModule = new JpaPersistModule("DefaultUnit");
        jpaModule.properties(properties);

        return jpaModule;
    }
}
