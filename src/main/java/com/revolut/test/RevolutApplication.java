package com.revolut.test;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.revolut.test.db.PersistInitialiser;
import com.revolut.test.resources.AccountResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class RevolutApplication extends Application<RevolutConfiguration> {

    public static void main(final String[] args) throws Exception {
        new RevolutApplication().run(args);
    }

    @Override
    public String getName() {
        return "revolut";
    }

    @Override
    public void initialize(final Bootstrap<RevolutConfiguration> bootstrap) {
        //No op
    }

    @Override
    public void run(final RevolutConfiguration configuration,
                    final Environment environment) {
        RevolutModule revolutModule = new RevolutModule(configuration, environment);
        final Injector injector = Guice.createInjector(revolutModule);
        injector.getInstance(PersistInitialiser.class);
        environment.jersey().register(injector.getInstance(AccountResource.class));
    }

}
