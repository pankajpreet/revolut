package com.revolut.test.resources.input;

import java.math.BigDecimal;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class TransferRO {
    @NotNull
    private Long fromAccount;
    @NotNull
    private Long toAccount;
    @NotNull
    private BigDecimal amount;
    @NotNull
    @NotBlank
    private String msg;
}
