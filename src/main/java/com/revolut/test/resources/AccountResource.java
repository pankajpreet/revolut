package com.revolut.test.resources;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;


import com.revolut.test.api.AccountManager;
import com.revolut.test.exception.ExternalException;
import com.revolut.test.exception.InvalidInputException;
import com.revolut.test.model.AccountBO;
import com.revolut.test.model.AccountStatus;
import com.revolut.test.model.TransferBO;
import com.revolut.test.resources.input.AccountRO;
import com.revolut.test.resources.input.TransferRO;
import com.revolut.test.resources.mapper.AccountMapper;
import com.revolut.test.resources.mapper.AccountTransferMapper;
import com.revolut.test.resources.output.Error;
import com.revolut.test.resources.output.Errors;
import java.math.BigDecimal;
import java.util.Set;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api/v1/accounts")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {

    @Inject
    private AccountManager accountManager;

    @POST
    public Response createAccount(AccountRO accountDetails) {
        try {
            validate(accountDetails);
        } catch (InvalidInputException e) {
            return Response.status(BAD_REQUEST).entity(new Errors(
                new Error(BAD_REQUEST.getStatusCode(), e.getMessage()))).build();
        }
        try {
            AccountBO account = accountManager.createAccount(AccountMapper.map(accountDetails));
            return Response.status(Response.Status.CREATED).entity(AccountMapper.mapReverse(account)).build();
        } catch (Exception e) {
            return Response.status(INTERNAL_SERVER_ERROR)
                .entity(new Errors(
                    new Error(INTERNAL_SERVER_ERROR.getStatusCode(), e.getMessage()))).build();
        }
    }

    @GET
    @Path("/{id}")
    public Response getAccount(@PathParam("id") Long accountNumber) {
        try {
            checkpermissions(accountNumber);
            AccountBO accountBO = accountManager.getAccoundById(accountNumber);
            if (null == accountBO) {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            return Response.ok().entity(AccountMapper.mapReverse(accountBO)).build();
        } catch (Exception e) {
            return Response.status(INTERNAL_SERVER_ERROR)
                .entity(new Errors(
                    new Error(INTERNAL_SERVER_ERROR.getStatusCode(), e.getMessage()))).build();
        }
    }

    @POST
    @Path("/transfer")
    public Response transfer(TransferRO transferRequest) throws ExternalException {
        //checkpermissions();
        //validate from account and toAccount, balanace is valid value
        try {
            validateTransferRequest(transferRequest);

            TransferBO transferBO = accountManager.transfer(AccountTransferMapper.map(transferRequest));
            return Response.ok().entity(AccountTransferMapper.mapReverse(transferBO)).build();
        } catch (InvalidInputException e) {
            return Response.status(BAD_REQUEST).entity(new Errors(
                new Error(BAD_REQUEST.getStatusCode(), e.getMessage()))).build();
        } catch (Exception e) {
            return Response.status(INTERNAL_SERVER_ERROR)
                .entity(new Errors(
                    new Error(INTERNAL_SERVER_ERROR.getStatusCode(), e.getMessage()))).build();
        }
    }

    private void validateTransferRequest(TransferRO transferRequest) throws ExternalException, InvalidInputException {
        Set<ConstraintViolation<TransferRO>> validationResult = getValidator().validate(transferRequest);
        if (!validationResult.isEmpty()) {
            throw new InvalidInputException(validationResult.toString());
        }
        validateAccountStatus(transferRequest.getFromAccount(), "fromAccount");
        validateAccountStatus(transferRequest.getFromAccount(), "toAccount");
        if (transferRequest.getAmount().compareTo(BigDecimal.ONE) < 0) {
            throw new InvalidInputException(String.format("Invalid input for field: %s", "amount"));
        }
    }

    private void validateAccountStatus(Long accountId, String fieldName)
        throws ExternalException, InvalidInputException {
        AccountBO account = accountManager.getAccoundById(accountId);
        if (null == account || account.getStatus() != AccountStatus.ACTIVE) {
            throw new InvalidInputException(String.format("Invalid input for field: %s", fieldName));
        }
    }

    private void validate(AccountRO accountDetails) throws InvalidInputException {
        Set<ConstraintViolation<AccountRO>> results = getValidator().validate(accountDetails);
        if (!results.isEmpty()) {
            throw new InvalidInputException(results.toString());
        }
    }

    private Validator getValidator() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        return factory.getValidator();
    }

    private void checkpermissions(Long accountNumber) {
        // Check permissions of the log in user, If he is the owner or he has rights to view the account
        //can transfer and other
    }


}
