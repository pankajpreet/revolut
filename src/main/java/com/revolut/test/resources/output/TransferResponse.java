package com.revolut.test.resources.output;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class TransferResponse {
    private Long transactionId;
    private Long fromAccount;
    private Long toAccount;
    private BigDecimal ammount;
    private BigDecimal availableBalance;
    private String msg;
}
