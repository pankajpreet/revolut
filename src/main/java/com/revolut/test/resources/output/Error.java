package com.revolut.test.resources.output;

import lombok.Data;

@Data
public class Error {
    private int errorCode;
    private String errorMsg;

    public Error(int errorCode, String message) {
        this.errorCode = errorCode;
        this.errorMsg = message;
    }
}
