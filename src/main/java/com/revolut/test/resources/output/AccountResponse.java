package com.revolut.test.resources.output;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class AccountResponse {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String mobileNumber;
    private BigDecimal balance;
}
