package com.revolut.test.resources.output;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

@Data
public class Errors {
    List<Error> data = new ArrayList<>();

    public Errors(Error error) {
        data.add(error);
    }

    public List<Error> getData() {
        return data;
    }

}
