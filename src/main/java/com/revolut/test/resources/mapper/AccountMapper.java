package com.revolut.test.resources.mapper;

import com.revolut.test.model.AccountBO;
import com.revolut.test.resources.input.AccountRO;
import com.revolut.test.resources.output.AccountResponse;

public class AccountMapper {

    public static AccountBO map(AccountRO accountDetails) {
        return AccountBO.builder().firstName(accountDetails.getFirstName())
            .lastName(accountDetails.getLastName())
            .email(accountDetails.getEmail())
            .mobileNumber(accountDetails.getMobileNumber()).build();
    }

    public static AccountResponse mapReverse(AccountBO accountDetails) {
        AccountResponse response = new AccountResponse();
        response.setId(accountDetails.getId());
        response.setFirstName(accountDetails.getFirstName());
        response.setLastName(accountDetails.getLastName());
        response.setEmail(accountDetails.getEmail());
        response.setMobileNumber(accountDetails.getMobileNumber());
        response.setBalance(accountDetails.getBalance());
        return response;
    }
}
