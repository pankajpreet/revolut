package com.revolut.test.resources.mapper;

import com.revolut.test.model.TransferBO;
import com.revolut.test.resources.input.TransferRO;
import com.revolut.test.resources.output.TransferResponse;

public class AccountTransferMapper {

    public static TransferBO map(TransferRO transferRequest) {
        return TransferBO.builder().fromAccount(transferRequest.getFromAccount()).toAccount(
            transferRequest.getToAccount())
            .amount(transferRequest.getAmount()).msg(transferRequest.getMsg()).build();
    }

    public static TransferResponse mapReverse(TransferBO transferBO) {
        TransferResponse transferRO = new TransferResponse();
        transferRO.setTransactionId(transferBO.getTransactionId());
        transferRO.setFromAccount(transferBO.getFromAccount());
        transferRO.setToAccount(transferBO.getToAccount());
        transferRO.setAmmount(transferBO.getAmount());
        transferRO.setMsg(transferBO.getMsg());
        transferRO.setAvailableBalance(transferBO.getAvailableBalance());
        return transferRO;
    }

}
