package com.revolut.test.exception;

public class ExternalException extends Exception {

    public ExternalException(String msg) {
        super(msg);
    }

    public ExternalException(String msg, Throwable throwable) {
        super(msg, throwable);
    }
}
