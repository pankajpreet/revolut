# revolut

How to start the revolut application
---

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/revolut-1.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`


# To keep setup Simple
// ID generation will be moved to dbase sequence

   accountBO.setId(IdGenerator.getInstance().getId());
   
//There are no main accounts, for now adding default balance for all accounts

   accountBO.setBalance(BigDecimal.TEN);
   
//By Default account should be INACTIVE and then another REST request to make the account ACTIVE
// These are done to keep it simple.

  accountBO.setStatus(AccountStatus.ACTIVE);

# Things can be improved
We can use two phase commit on balances like reserve and then commit. So that we can track actual and available balance. Transaction History, etc

Optimistic locking should be introduced for concurrent scenarios. so that always latest version should be updated and no overriding of data should happen

Currently no logging and permissions checks


# Postman test case
Postman directory contain postman test case. Server should be running to execute these test case. These test cases can be executed using postman app.